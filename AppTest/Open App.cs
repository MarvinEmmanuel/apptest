﻿using System;
using System.IO;
using System.Linq;
using NUnit.Framework;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace AppTest
{
    [TestFixture(Platform.Android)]
    [TestFixture(Platform.iOS)]
    public class Tests
    {
        IApp app;
        Platform platform;
        public Tests(Platform platform)
        {
            this.platform = platform;
        }
        [SetUp]
        public void BeforeEachTest()
        {
            app = AppInitializer.StartApp(platform);
        }
        [Test]
        public void AppLaunches()
        {
            app.Screenshot(“First screen.“);
        }
        //Select Skip and then Basket
        [Test]
        public void FindElement()
        {
            app.Tap(e => e.Marked(“Skip”));
            app.Tap(e => e.Marked(“Basket”));
        }
    }
}
